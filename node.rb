class Node
  attr_accessor :children, :parent, :name, :color

  def initialize(name=nil, h={})
    self.children = []
    self.color = :black
    h.each {|k,v|self.send("#{k}=",v)}
    if name then self.name = name end
  end

  def add(child)
    self.children << child
    child.parent = self
    child
  end

  def blacken
    self.children.each{|c|c.blacken}
    self.color = :black
  end

  def has_branch?
    self.children.size > 1
  end

  def longest
    if children.empty? then [self]
    else
      result = []
      children.each{|child|
        longest_child = child.longest
        if longest_child.size >= result.size then result = [self]+longest_child end
      }
      result
    end
  end

  def longest_for_iupac
    normal = sum_of_branch_indices
    reversed = sum_of_branch_indices(:reverse=>true)
    if normal <= reversed then longest
    else longest.reverse end
  end

  def sum_of_branch_indices(options={})
    root = options[:reverse] ? longest.reverse : longest
    total = 0
    root.each_with_index {|node, i|
      if node.has_branch? then total += i end
    }
    total
  end

  def to_s
    if parent then "<p:#{parent.name}, c:#{children}>"
    else "c:#{children}" end
  end
end