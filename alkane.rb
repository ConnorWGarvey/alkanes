require './node'

class Alkane
  ONES_C_COUNT = [
    'meth',
    'eth',
    'prop',
    'but',
    'pent',
    'hex',
    'hept',
    'oct',
    'non'
  ]
  C_COUNT = [
    'hen',
    'do',
    'tri',
    'tetra',
    'penta',
    'hexa',
    'hepta',
    'octa',
    'nona'
  ]
  COUNT = [nil,
    '',
    'di',
    'tri',
    'tetra',
    'penta',
    'hexa',
    'hepta',
    'octa',
    'nona',
    'deca'
  ]

  attr_accessor :tree

  def initialize(h={})
    h.each {|k,v|self.send("#{k}=",v)}
  end

  def self.smiles(smiles)
    alkane = Alkane.new(:tree=>Node.new(:name=>'r'))
    node = alkane.tree
    branches = []
    branched = true
    smiles.split('').each_with_index {|c,i|
      if c == 'C' then
        if branched then branched = false
        else node = node.add(Node.new(:name=>i)) end
      elsif c == '('
        branches << node
        node = node.add(Node.new(:name=>i))
        branched = true
      elsif c == ')' then node = branches.pop end
    }
    alkane
  end

  def to_iupac
    chain = tree.longest_for_iupac
    chain.first.blacken
    chain.last.blacken
    chain.each{|n|n.color = :red}
    branch_names = name_branches(chain)
    "#{branch_names}#{simple_name(chain.size)}".capitalize
  end

  private

  def apply_iupac(count, name, segment_count, segment_name)
    if count >= segment_count
      count -= segment_count
      return [count, prepend(name, segment_name)]
    end
    [count, name]
  end

  def branch_to_iupac(root)
    chain = root.longest_for_iupac
    branch_names = name_branches(chain)
    "#{branch_names}#{simple_branch_name(chain.size)}".capitalize
  end

  def ends_with_vowel?(text)
    if text.empty? then return false end
    is_vowel?(text[text.length-1])
  end

  def is_vowel?(char)
    ['a','e','i','o','u','y'].include?(char)
  end

  def iupac_carbon_count(count)
    if count < 10
      return ONES_C_COUNT[count - 1]
    end
    result = ''
    if count > 100
      result = prepend(result, 'hect')
      count -= 100
      if count == 1
        result = "heni#{result}"
        count -= 1
      end
    end
    count, result = apply_iupac(count, result, 100, 'hecta')
    count, result = apply_iupac(count, result, 90, 'nonaconta')
    count, result = apply_iupac(count, result, 80, 'octaconta')
    count, result = apply_iupac(count, result, 70, 'heptaconta')
    count, result = apply_iupac(count, result, 60, 'hexaconta')
    count, result = apply_iupac(count, result, 50, 'pentaconta')
    count, result = apply_iupac(count, result, 40, 'tetraconta')
    count, result = apply_iupac(count, result, 30, 'triaconta')
    count, result = apply_iupac(count, result, 20, 'icosa')
    if count > 9
      result = prepend(result, 'deca')
      count -= 10
      if count == 1
        result = "un#{result}"
        count -= 1
      end
    end
    if count != 0
      result = prepend(result, C_COUNT[count - 1])
    end
    result
  end

  def name_branches(chain)
    branches = {}
    chain.each_with_index {|node, i|
      if node.has_branch?
        node.children.each{|child|
          if child.color != :red
            name = branch_to_iupac(child)
            if branches[name]
              branch = branches[name]
              branch[:count] = branch[:count] + 1
              branch[:indices] = branch[:indices] + [i+1]
            else branches[name] = {:count=>1, :indices=>[i+1]} end
            child.color = :red
          end
        }
      end
    }
    branches = branches.collect{|k,v|[k,v]}.sort{|b|b[0]}
    branch_names = branches.collect{|b|"#{COUNT[b[1][:count]]}#{b[0]}"}.join
    indices = branches.collect{|b|b[1][:indices]}.flatten.join(',')
    chain.each{|n|n.color = :red}
    indices.empty? ? branch_names : "#{indices}-#{branch_names}"
  end

  def prepend(text, prefix)
    if text.empty?
      return ends_with_vowel?(prefix) ? prefix[0..-2] : prefix
    end
    if ends_with_vowel?(prefix) && starts_with_vowel?(text) then text = text[1..-1] end
    "#{prefix}#{text}"
  end

  def simple_branch_name(count)
    simple_name_helper(count, 'yl')
  end

  def simple_name(count)
    simple_name_helper(count, 'ane')
  end

  def simple_name_helper(count, suffix)
    "#{iupac_carbon_count(count)}#{suffix}"
  end

  def starts_with_vowel?(text)
    if text.empty? then return false end
    is_vowel?(text[0])
  end
end