require 'rspec'
require './alkane'

describe 'convert' do
  it 'should convert straight chain SMILES to IUPAC for 1 to 40 carbons' do
    names = ['Methane','Ethane','Propane','Butane','Pentane','Hexane','Heptane','Octane','Nonane','Decane','Undecane',
        'Dodecane','Tridecane','Tetradecane','Pentadecane','Hexadecane','Heptadecane','Octadecane','Nonadecane',
        'Icosane','Henicosane','Docosane','Tricosane','Tetracosane','Pentacosane','Hexacosane','Heptacosane',
        'Octacosane','Nonacosane','Triacontane','Hentriacontane','Dotriacontane','Tritriacontane','Tetratriacontane',
        'Pentatriacontane','Hexatriacontane','Heptatriacontane','Octatriacontane','Nonatriacontane','Tetracontane']
    names.each_with_index{|expected,i|expect(Alkane.smiles('C'*(i+1)).to_iupac).to eq(expected)}
  end

  it 'should convert straight chain SMILES to IUPAC' do
    names = {50=>'Pentacontane',60=>'Hexacontane',70=>'Heptacontane',80=>'Octacontane',90=>'Nonacontane',100=>'Hectane',
        101=>'Henihectane',110=>'Decahectane',111=>'Undecahectane',120=>'Icosahectane',121=>'Henicosahectane'}
    names.each{|n,expected|expect(Alkane.smiles('C'*n).to_iupac).to eq(expected)}
  end
  
  it 'should fail for really long chains that it can not name' do
    expect(Alkane.smiles('C'*11)).to raise_error
  end

  it 'should convert branched SMILES to IUPAC' do
    expecteds = {
        'CCC(C)C'=>'2-methylbutane',
        'CC(C)CC'=>'2-methylbutane',
        'CC(C)(C)CC'=>'2,2-dimethylbutane',
        'C(C)C(C)(C)C(C)C'=>'1,2,2,3-tetramethylbutane'
    }
    expecteds.each{|k,v|expect(Alkane.smiles(k).to_iupac).to eq(v)}
  end
end