require 'rspec'
require './alkane'
require './node'

describe 'longest' do
  it 'should find the longest branch path' do
    n0 = Node.new('0')
    n0.add(Node.new('0a'))
    n1 = n0.add(Node.new('1'))
    n1a = n1.add(Node.new('1a'))
    n1.add(Node.new('2'))
    expect(n0.longest).to eq([n0, n1, n1a]) # This may also be [0,1,2], but don't have time to optimize test
  end
end

describe 'longest for iupac' do
  it 'should pick the correct order' do
    n0 = Node.new('0')
    n1 = n0.add(Node.new('1'))
    n2 = n1.add(Node.new('2'))
    n3 = n2.add(Node.new('3'))
    n3.add(Node.new('3a'))
    n4 = n3.add(Node.new('4'))
    n5 = n4.add(Node.new('5'))
    expect(n0.longest_for_iupac).to eq([n5, n4, n3, n2, n1, n0])
  end
end

describe 'sum of branch indices' do
  it 'should add correctly' do
    cases = {'C'=>0, 'C(C)C(C)'=>0, 'C(C)C(C)C'=>1, 'CCC(C)CC'=>2, 'CCC(C(C)C)CC(C)'=>2, 'CCC(C(C)C)C(C)C'=>5}
    cases.each {|k,v|
      expect(Alkane.smiles(k).tree.sum_of_branch_indices).to eq(v)
    }
  end
  it 'should sum the reverse if requested' do
    cases = {'C'=>0, 'C(C)C(C)'=>2, 'C(C)C(C)C'=>3, 'CCC(C)CC'=>2, 'CCC(C(C)C)CC(C)'=>3, 'CCC(C(C)C)C(C)C'=>3}
    cases.each {|k,v|
      expect(Alkane.smiles(k).tree.sum_of_branch_indices(:reverse=>true)).to eq(v)
    }
  end
end